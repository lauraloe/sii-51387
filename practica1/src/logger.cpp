//se crea la tuberia fifo abriendola en modo lectura

//se destruye: close y unlink

#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
	
	//Crear tuberia

	unlink("/tmp/loggerfifo");	

	if(mkfifo("/tmp/loggerfifo", 0777)==-1) //hay error
	{
		printf("Error al crear FIFO del logger");
		exit(1);
	}
	
	else
	{
		printf("FIFO del logger creada");
	}

	//Abrir tuberia en modo lectura
	
	int fd = open("/tmp/loggerfifo", O_RDONLY); 

	if(fd==-1)
	{

		
		perror("Error al abrir FIFO del logger");
		exit(1);
	}
	else
		printf("Abierta FIFO del logger con exito");

	char buff[250];	
	int nLeido;

	while(1)
	{
		nLeido=read(fd, buff, sizeof(buff));
		if(nLeido==-1){
			perror("Error al leer FIFO del logger");
			exit(1);
		}
		else if(buff[0]=='-'){

			printf("%s\n", buff);
			break;
		}

		else
			printf("%s\n", buff);
	}

	close(fd); //cierra descriptor fichero
	unlink("/tmp/loggerfifo"); //elimina fifo
	return 0;
}
		
