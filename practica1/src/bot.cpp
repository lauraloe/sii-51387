#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include "DatosMemCompartida.h"

int main(){

	int file;
	DatosMemCompartida *pbot;
	char *proyeccion;

	file=open("/tmp/MemCompartida", O_RDWR);
	if (file==-1)
	{
		perror("Error al abrir fichero para la memoria compartida");
		close(file);
		return -1;
	}

	pbot=(DatosMemCompartida*)mmap(NULL, sizeof(*(pbot)), PROT_WRITE|PROT_READ, MAP_SHARED, file,0);

	if(pbot==MAP_FAILED)
	{
	perror("No se puede abrir el fichero a proyectar");
		return -1;
	}

	close(file);

	
	while(1)
	{

	float ptoCentralR1;
	float ptoCentralR2;

	ptoCentralR1=((pbot->raqueta1.y1+pbot->raqueta1.y2)/2);

	ptoCentralR2=((pbot->raqueta2.y1+pbot->raqueta2.y2)/2);

	 if(ptoCentralR1<pbot->esfera.centro.y)
		pbot->accion1=1; //subir

	else if(ptoCentralR1>pbot->esfera.centro.y)
		pbot->accion1=-1; //bajar

	else
		pbot->accion1=0;//nada


	if(ptoCentralR2<pbot->esfera.centro.y)
		pbot->accion2=1; //subir

	else if(ptoCentralR2>pbot->esfera.centro.y)
		pbot->accion2=-1; //bajar

	else
		pbot->accion2=0;//nada


	usleep(25000);

	}

	munmap(proyeccion,sizeof(*(pbot)));

	return 1;
	
}

