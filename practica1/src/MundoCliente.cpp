// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{

	char salir[150];
	
	sprintf(salir, "---FIN---");
	
	/*
	//Cerrar FIFO servidor-cliente
	write(fd_sc, salir, strlen(salir)+1);
	close(fd_sc);
	unlink("/tmp/FIFOservidor");

	//Cerrar FIFO cliente-servidor
	write(fd_cs, salir, strlen(salir)+1);
	close(fd_cs);
	unlink("/tmp/FIFOcliente");
	*/
	
	munmap(proyeccion, sizeof(bot));
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	
	//codigo bot

	pbot->esfera=esfera;
	pbot->raqueta1=jugador1;
	pbot->raqueta2=jugador2;

	if(pbot->accion1==1) OnKeyboardDown('w',0,0);
	else if (pbot->accion1==-1) OnKeyboardDown('s',0,0);
	else if (pbot->accion1==0);

	if(pbot->accion2==1) OnKeyboardDown('o',0,0);
	else if (pbot->accion2==-1) OnKeyboardDown('l',0,0);
	else if (pbot->accion2==0);
	

	//Lectura de los datos de la FIFO servidor-cliente
	
	/*
	fd2=read(fd_sc, cad, sizeof(cad));
	if(fd2==-1)
	{
		//unlink("/tmp/FIFOservidor");
		printf("Error al leer la FIFO servidor-cliente");
		exit(1);
	}

	else;
	*/

	
	char coord[150];	
	socket_comunicacionservidor.Receive(coord, sizeof(coord));

	sscanf (coord, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);

	//Implementacion para que el juego acabe cuando uno de los tres jugadores alcance los 3 puntos
	char cad1[150];

	if((puntos1==3)||(puntos2==3))
		exit(1);
}


void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char cad[150];
	sprintf(cad, "%c", key);
	//write(fd_cs, cad, strlen(cad)+1);

	socket_comunicacionservidor.Send(cad, sizeof(cad));

}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
/*
//Crear y abrir la tuberia servidor-cliente en modo lectura
	
	unlink("/tmp/FIFOservidor"); //Como precaucion se elimina por si ya existe	

	int fd1;	

	fd1=mkfifo("/tmp/FIFOservidor", 0666);
	if(fd1==-1)
	{
		perror("Error al crear la FIFO servidor-cliente");
	}
	
	else
		printf("se ha creado la FIFO servidor-cliente correctamente");

	fd_sc=open("/tmp/FIFOservidor", O_RDONLY);
	if(fd_sc==-1)
	{
		perror("Error al abrir la FIFO servidor-cliente2");
	}
	else
		printf("Se ha abierto la FIFO servidor-cliente correctamente");

//FIN CREAR Y ABRIR FIFO S-C

//Crear y abrir la FIFO cliente-servidor

	unlink("/tmp/FIFOcliente"); //Como precaucion se elimina por si ya existe
	
	int fd2;
	fd2=mkfifo("/tmp/FIFOcliente",0666);
	if(fd2==-1)
	{
		perror("Error al crear la FIFO cliente-servidor");
	}
	else 
		printf("se ha creado correctamente la FIFO cliente-servidor");

	fd_cs=open("/tmp/FIFOcliente", O_WRONLY);
	if(fd_cs==-1)
	{
		perror("Error al abrir la FIFO cliente-servidor");
	}
//FIN CREAR Y ABRIR FIFO C-S
*/

	//codigo bot	

	fd=open("/tmp/MemCompartida", O_RDWR|O_CREAT|O_TRUNC, 0666);
	if(fd==-1)
	{

		printf("\n No se puede abrir el fichero para proyectar");
		close(fd);
		exit(1);	
	}

	write(fd, &bot, sizeof(bot));

	//Proyeccion de memoria

	proyeccion=(char*)mmap(NULL, sizeof(bot), PROT_WRITE|PROT_READ, MAP_SHARED, fd, 0);
	
	if(proyeccion==MAP_FAILED){
		perror("NO se puede abrir el fichero de proyeeccion");
		exit(1);
	}

	close(fd);
	pbot=(DatosMemCompartida*)proyeccion;
	pbot->accion1=0;
	pbot->accion2=0;

	//Sockets
	char nom[250];
	printf("Introduzca nombre: ");
	scanf("%s", nom);

	socket_comunicacionservidor.Connect((char *) "127.0.0.1",2500);
	socket_comunicacionservidor.Send(nom, sizeof(nom));
	
}
