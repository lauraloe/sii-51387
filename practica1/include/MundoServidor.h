// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//Cabecera modificada por Laura Ortega Espinosa: lauraloe 


#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <pthread.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <sys/mman.h>
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	
	int fd_logger; //fifo logger
	//int fd_sc; //descriptor de la fifo servidor-cliente
	int fd_cs; //descriptor de la fifo cliente-servidor
	int TUBERIA_CREADA;
	
	pthread_t thread;

	int puntos1;
	int puntos2;

	Socket socket_conexioncliente;
	Socket socket_comunicacioncliente;

	
	int cont = 250;
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
