// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////
//Cabecera modificada por Laura Ortega Espinosa: lauraloe 


#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <sys/mman.h>
#include "DatosMemCompartida.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int fd; //descriptor de la fifo

	int puntos1;
	int puntos2;

	DatosMemCompartida bot;
	DatosMemCompartida *pbot;
	char *proyeccion;

	int cont = 250;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
