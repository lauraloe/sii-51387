// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
//Cabecera modificada por Laura Ortega Espinosa: lauraloe 


#include "Plano.h"
#include "Vector2D.h"

class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);
};
